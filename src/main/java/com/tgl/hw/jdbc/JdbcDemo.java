package com.tgl.hw.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import com.tgl.hw.jdbc.controller.EmployeeController;
import com.tgl.hw.jdbc.model.Employee;
import com.tgl.hw.jdbc.util.EmployeeNotValidException;

/**
 * 
 * @author kite.chen Designed main demo flow
 *
 */
public class JdbcDemo {

  private EmployeeController employeeController;
  
  public static void main(String[] args) throws InterruptedException {

    String filePath = "";
    String dbUrl = "";
    String dbUsername = "";
    String dbPassword = "";

    try (InputStream input =
        JdbcDemo.class.getClassLoader().getResourceAsStream("config.properties")) {
      Properties prop = new Properties();
      prop.load(input);
      filePath = prop.getProperty("employee.data");
      dbUrl = prop.getProperty("db.connection");
      dbUsername = prop.getProperty("db.username");
      dbPassword = prop.getProperty("db.password");
    } catch (IOException ex) {
      ex.printStackTrace();
    }

    try (Connection con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword)) {

      JdbcDemo jdbcDemo = new JdbcDemo();
      jdbcDemo.employeeController = new EmployeeController(con, filePath);

      System.out.println("\n=============search case [by ID] fail==========");
      jdbcDemo.search("100");

      System.out.println("\n=============search case [by ID] success==========");
      jdbcDemo.search("3");
      
      System.out.println("\n=============add case success==========");
      Employee employee = new Employee();
      employee.setHeight(173.5);
      employee.setWeight(73);
      employee.setEngName("Kite Chen");
      employee.setChName("陳銘大凱");
      employee.setPhone("6926");
      employee.setEmail("kitechen@transglobe.com.tw");
      jdbcDemo.insert(employee);

      // make the update time different
      Thread.sleep(3000);
      
      System.out.println("\n=============update case success==========");
      String employeeId = "57";
      List<Employee> results = jdbcDemo.employeeController.search(Employee.Search.ID, employeeId);
      if(results != null && !results.isEmpty()) {        
        employee = results.get(0);
        employee.setHeight(180);
        employee.setChName("陳銘大凱"); // recover mask name
        boolean isUpdated = false;
        try {
          isUpdated = jdbcDemo.employeeController.update(employee);
        } catch (EmployeeNotValidException e) {
          e.printStackTrace();
        }
        if(isUpdated) {        
          System.out.println("employee updated: " + employee);
        }
      }
      
      System.out.println("\n=============delete [by ID] case fail==========");
      jdbcDemo.delete(100);

      System.out.println("\n=============delete [by Id] case success==========");
      jdbcDemo.delete(57);

      System.out.println("\n=============sort [by phone] case (print only first 10)==========");
      List<Employee> resultList = fileIODemo.employeeController.sort(Employee.Sort.PHONE);
      int showCnt = 10;
      for (Employee result : resultList) {
        if (showCnt > 0) {
          System.out.printf("%s, %s %n", result.getChName(), result.getPhone());
        } else {
          break;
        }
        showCnt--;
      }

      System.out.println("\n=============max [by height] case ==========");
      employee = jdbcDemo.employeeController.max(Employee.Max.HEIGHT);
      System.out.printf("%s, %s %n", employee.getChName(), employee.getHeight());

      System.out.println("\n=============min [by height] case ==========");
      employee = jdbcDemo.employeeController.min(Employee.Min.HEIGHT);
      System.out.printf("%s, %s %n", employee.getChName(), employee.getHeight());

      System.out.println("\n=============TRUNCATE employee success==========");
      fileIODemo.employeeController.truncate();
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
  }

  public void search(String employeeId) {
    List<Employee> results = employeeController.search(Employee.Search.ID, employeeId);
    if (results == null || results.isEmpty()) {
      System.out.printf("employee Id[%s] not found%n", employeeId);
    } else {
      for(Employee employee : results) {        
        System.out.println("search result by Id: " + employee);
      }
    }
  }

  public void insert(Employee employee) {
    try {
      employee = employeeController.insert(employee);
    } catch (EmployeeNotValidException e) {
      e.printStackTrace();
    }
    System.out.println("employee added: " + employee);
  }

  public void delete(long deleteId) {
    System.out.println("employee size defore delete: " + employeeController.size());
    boolean isDeleted = employeeController.delete(deleteId);
    if (isDeleted) {
      System.out.printf("employee deleted by Id: %s%n", deleteId);
    } else {
      System.out.printf("employee Id[%s] not found%n", deleteId);
    }
    System.out.println("employee size after delete: " + employeeController.size());
  }
}
