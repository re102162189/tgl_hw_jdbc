package com.tgl.hw.jdbc.model;

public final class EmployColumon {

  private EmployColumon() {}

  public static final String ID = "id";
  public static final String CH_NAME = "ch_name";
  public static final String ENG_NAME = "eng_name";
  public static final String EMAIL = "email";
  public static final String PHONE = "phone";
  public static final String HEIGHT = "height";
  public static final String WEIGHT = "weight";
  public static final String BMI = "bmi";
}
