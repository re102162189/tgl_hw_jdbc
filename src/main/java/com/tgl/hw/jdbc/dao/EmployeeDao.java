package com.tgl.hw.jdbc.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.tgl.hw.jdbc.mapper.EmployeeMapper;
import com.tgl.hw.jdbc.model.Employee;
import com.tgl.hw.jdbc.util.DataUtil;
import com.tgl.hw.jdbc.util.EmployeeNotValidException;

/**
 * 
 * @author kite.chen Designed for data access
 *
 */
public class EmployeeDao {

  private static final int BATCH_SIZE = 30;

  public enum Condition {
    AND, OR
  }

  public enum ORDER {
    ASC, DESC
  }

  private static final String INSERT =
      "INSERT INTO employee (ch_name, eng_name, email, height, weight, bmi, phone) VALUES (?, ?, ?, ?, ?, ?, ?)";

  private static final String SELECT =
      "SELECT id, ch_name, eng_name, email, height, weight, bmi, phone FROM employee";

  private static final String UPDATE =
      "UPDATE employee SET ch_name = ?,  eng_name = ?, email = ?, height = ?, weight = ?, bmi = ?, phone = ? WHERE id = ?";

  private static final String DELETE = "DELETE FROM employee WHERE id = ?";

  private static final String COUNT = "SELECT count(*) AS count FROM employee";

  private static final String TRUNCATE = "TRUNCATE employee";

  private Connection con;

  private EmployeeMapper employeeMapper;

  public EmployeeDao(Connection con, String filePath) {
    this.con = con;
    this.employeeMapper = new EmployeeMapper();
    try (InputStream input = EmployeeDao.class.getClassLoader().getResourceAsStream(filePath);
        BufferedReader reader =
            new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));) {
      String line;
      String[] data;
      List<Employee> employees = new ArrayList<>();
      while ((line = reader.readLine()) != null) {
        data = line.split(" ");
        Employee employee = new Employee();
        employee.setHeight(Integer.parseInt(data[0]));
        employee.setWeight(Integer.parseInt(data[1]));
        employee.setEngName(data[2]);
        employee.setChName(data[3]);
        employee.setPhone(data[4]);
        employee.setEmail(data[5]);
        employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
        employees.add(employee);
      }
      batchInsert(employees);
      System.out.printf("=============init employee data, size: %s==========%n", employees.size());
    } catch (IOException | EmployeeNotValidException e1) {
      e1.printStackTrace();
    }
  }

  public int batchInsert(List<Employee> employees) {
    int rowCnt = 0;
    int[] resultRows = {};
    try (PreparedStatement stmt = this.con.prepareStatement(INSERT)) {
      for (Employee employee : employees) {
        this.employeeMapper.convertForInsert(employee, stmt);
        stmt.addBatch();
        if (++rowCnt % BATCH_SIZE == 0) {
          int[] affectedRows = stmt.executeBatch();
          resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
        }
      }
      if (rowCnt % BATCH_SIZE != 0) {
        int[] affectedRows = stmt.executeBatch();
        resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return resultRows.length;
  }

  public Employee insert(Employee employee) {
    try (PreparedStatement stmt =
        this.con.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
      this.employeeMapper.convertForInsert(employee, stmt);
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new SQLException("Creating employee failed, no rows affected.");
      }
      try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          employee.setId(generatedKeys.getLong(1));
        } else {
          throw new SQLException("Creating employee failed, no ID obtained.");
        }
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return employee;
  }

  public int batchDelete(long... employeeIds) {
    int rowCnt = 0;
    int[] resultRows = {};
    try (PreparedStatement stmt = this.con.prepareStatement(DELETE)) {
      for (long employeeId : employeeIds) {
        stmt.setLong(1, employeeId);
        stmt.addBatch();
        if (++rowCnt % BATCH_SIZE == 0) {
          int[] affectedRows = stmt.executeBatch();
          resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
        }
      }
      if (rowCnt % BATCH_SIZE != 0) {
        int[] affectedRows = stmt.executeBatch();
        resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return resultRows.length;
  }

  public boolean delete(long employeeId) {
    boolean isDeleted = false;
    try (PreparedStatement stmt = this.con.prepareStatement(DELETE)) {
      stmt.setLong(1, employeeId);
      int affectedRows = stmt.executeUpdate();
      isDeleted = affectedRows > 0;
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return isDeleted;
  }

  public int batchUpdate(List<Employee> employees) {
    int rowCnt = 0;
    int[] resultRows = {};
    try (PreparedStatement stmt = this.con.prepareStatement(UPDATE)) {
      for (Employee employee : employees) {
        this.employeeMapper.convertForUpdate(employee, stmt);
        stmt.addBatch();
        if (++rowCnt % BATCH_SIZE == 0) {
          int[] affectedRows = stmt.executeBatch();
          resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
        }
      }
      if (rowCnt % BATCH_SIZE != 0) {
        int[] affectedRows = stmt.executeBatch();
        resultRows = Arrays.copyOf(resultRows, affectedRows.length + 1);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return resultRows.length;
  }

  public boolean update(Employee employee) {
    boolean isUpdated = false;
    try (PreparedStatement stmt = this.con.prepareStatement(UPDATE)) {
      this.employeeMapper.convertForUpdate(employee, stmt);
      int affectedRows = stmt.executeUpdate();
      isUpdated = affectedRows > 0;
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return isUpdated;
  }

  public List<Employee> search(Employee.Search factor, String searchVal) {
    String query = null;
    switch (factor) {
      case ID:
        query = SELECT + " WHERE id = ?";
        break;
      case EMAIL:
        query = SELECT + " WHERE email = ?";
        break;
      case PHONE:
        query = SELECT + " WHERE phone = ?";
        break;
      default:
        break;
    }
    if (query == null) {
      return new ArrayList<>();
    }
    List<Employee> results = new ArrayList<>();
    try (PreparedStatement stmt = this.con.prepareStatement(query)) {
      stmt.setLong(1, Long.parseLong(searchVal));
      try (ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          Employee employee = this.employeeMapper.convertToModel(rs);
          results.add(employee);
        }
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return results;
  }

  public List<Employee> sort(Employee.Sort factor) {
    String query = null;
    switch (factor) {
      case PHONE:
        query = SELECT + " ORDER BY phone";
        break;
      case ENG_NAME:
        query = SELECT + " ORDER BY eng_name";
        break;
      default:
        break;
    }
    if (query == null) {
      return new ArrayList<>();
    }
    List<Employee> results = new ArrayList<>();
    try (PreparedStatement stmt = this.con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        Employee employee = this.employeeMapper.convertToModel(rs);
        results.add(employee);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return results;
  }

  public Employee max(Employee.Max factor) {
    String query = null;
    switch (factor) {
      case HEIGHT:
        query = SELECT + " ORDER BY height DESC LIMIT 1";
        break;
      case WEIGHT:
        query = SELECT + " ORDER BY weight DESC LIMIT 1";
        break;
      case BMI:
        query = SELECT + " ORDER BY bmi DESC LIMIT 1";
        break;
      default:
        break;
    }
    if (query == null) {
      return new Employee();
    }
    Employee result = new Employee();
    try (PreparedStatement stmt = this.con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        result = this.employeeMapper.convertToModel(rs);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return result;
  }

  public Employee min(Employee.Min factor) {
    String query = null;
    switch (factor) {
      case HEIGHT:
        query = SELECT + " ORDER BY height ASC LIMIT 1";
        break;
      case WEIGHT:
        query = SELECT + " ORDER BY weight ASC LIMIT 1";
        break;
      case BMI:
        query = SELECT + " ORDER BY bmi ASC LIMIT 1";
        break;
      default:
        break;
    }
    if (query == null) {
      return new Employee();
    }
    Employee result = new Employee();
    try (PreparedStatement stmt = this.con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        result = this.employeeMapper.convertToModel(rs);
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return result;
  }

  public int size() {
    int size = 0;
    try (PreparedStatement stmt = this.con.prepareStatement(COUNT);
        ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        size = rs.getInt("count");
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return size;
  }

  public boolean truncate() {
    boolean isTruncated = false;
    try (PreparedStatement stmt = this.con.prepareStatement(TRUNCATE)) {
      isTruncated = stmt.execute();
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    return isTruncated;
  }
}
