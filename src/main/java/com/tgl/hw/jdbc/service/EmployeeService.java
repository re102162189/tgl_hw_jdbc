package com.tgl.hw.jdbc.service;

import java.sql.Connection;
import java.util.List;
import com.tgl.hw.jdbc.dao.EmployeeDao;
import com.tgl.hw.jdbc.model.Employee;
import com.tgl.hw.jdbc.util.DataUtil;
import com.tgl.hw.jdbc.util.EmployeeNotValidException;

/**
 * 
 * @author kite.chen Designed for business logic
 *
 */
public class EmployeeService {

  private EmployeeDao employeeDao;

  public EmployeeService(Connection con, String filePath) {
    employeeDao = new EmployeeDao(con, filePath);
  }

  public Employee insert(Employee employee) throws EmployeeNotValidException {
    employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
    return employeeDao.insert(employee);
  }

  public boolean delete(long employeeId) {
    return employeeDao.delete(employeeId);
  }

  public boolean update(Employee employee) {
    return employeeDao.update(employee);
  }

  public List<Employee> search(Employee.Search factor, String searchVal) {
    List<Employee> results = employeeDao.search(factor, searchVal);
    for (Employee result : results) {
      String chName = result.getChName();
      String maskedName = DataUtil.maskChName(chName);
      result.setChName(maskedName);
    }
    return results;
  }

  public List<Employee> sort(Employee.Sort factor) {
    List<Employee> results = employeeDao.sort(factor);
    for (Employee result : results) {
      String chName = result.getChName();
      String maskedName = DataUtil.maskChName(chName);
      result.setChName(maskedName);
    }
    return results;
  }

  public Employee max(Employee.Max factor) {
    Employee result = employeeDao.max(factor);
    String chName = result.getChName();
    String maskedName = DataUtil.maskChName(chName);
    result.setChName(maskedName);
    return result;
  }

  public Employee min(Employee.Min factor) {
    Employee result = employeeDao.min(factor);
    String chName = result.getChName();
    String maskedName = DataUtil.maskChName(chName);
    result.setChName(maskedName);
    return result;
  }

  public int size() {
    return employeeDao.size();
  }

  public boolean truncate() {
    return employeeDao.truncate();
  }
}
