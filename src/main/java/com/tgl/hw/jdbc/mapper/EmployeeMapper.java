package com.tgl.hw.jdbc.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.tgl.hw.jdbc.model.EmployColumon;
import com.tgl.hw.jdbc.model.Employee;

public final class EmployeeMapper {
  public Employee convertToModel(ResultSet rs) throws SQLException {
    Employee employee = new Employee();
    employee.setId(rs.getLong(EmployColumon.ID));
    employee.setChName(rs.getString(EmployColumon.CH_NAME));
    employee.setEngName(rs.getString(EmployColumon.ENG_NAME));
    employee.setEmail(rs.getString(EmployColumon.EMAIL));
    employee.setPhone(rs.getString(EmployColumon.PHONE));
    employee.setHeight(rs.getDouble(EmployColumon.HEIGHT));
    employee.setWeight(rs.getDouble(EmployColumon.WEIGHT));
    employee.setBmi(rs.getDouble(EmployColumon.BMI));
    return employee;
  }
  
  public void convertForInsert(Employee employee, PreparedStatement stmt) throws SQLException {
    stmt.setString(1, employee.getChName());
    stmt.setString(2, employee.getEngName());
    stmt.setString(3, employee.getEmail());
    stmt.setDouble(4, employee.getHeight());
    stmt.setDouble(5, employee.getWeight());
    stmt.setDouble(6, employee.getBmi());
    stmt.setString(7, employee.getPhone());
  }
  
  public void convertForUpdate(Employee employee, PreparedStatement stmt) throws SQLException {
    stmt.setString(1, employee.getChName());
    stmt.setString(2, employee.getEngName());
    stmt.setString(3, employee.getEmail());
    stmt.setDouble(4, employee.getHeight());
    stmt.setDouble(5, employee.getWeight());
    stmt.setDouble(6, employee.getBmi());
    stmt.setString(7, employee.getPhone());
    stmt.setLong(8, employee.getId());
  }
}
